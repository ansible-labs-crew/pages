+++
title = "There is more to automation controller than the Web UI"
weight = 3
+++

This is an advanced automation controller lab so we don’t really want you to use the web UI for everything. To fully embrace automation and adopt the [infrastructure as code](https://en.wikipedia.org/wiki/Infrastructure_as_code) methodology, **we want to use Ansible to configure our automation controller** cluster.

Since automation controller is exposing all of its functionality via REST API, we can automate everything. Instead of using the API directly, it is highly recommended to use the [AWX](https://github.com/ansible/awx/tree/devel/awx_collection) or [automation controller](https://cloud.redhat.com/ansible/automation-hub/repo/published/ansible/controller) Ansible Collection (the second link will only work for you, if you have an active Red Hat Ansible Automation Platform Subscription) to setup, configure and maintain your Red Hat Ansible Automation Platform Cluster.

Execution environments, among other benefits, give us a reliable and predictable way to run our Ansible Playbook, since they come with Ansible, Python, our collections and all dependencies packaged together. Since the `ee-support-rhel8` which we are using already has the `ansible.controller` collection installed, we don't have to worry about that.

## Authentication

Before we can do any changes on our automation controller, we have to authenticate our user. There are several methods available to provide authentication details to the modules. In this lab, we'll use environment variables. In your VSCode UI use either your preferred editor from the command line or the visual editor (create a new file by going to **File**, **New File**) to create a new file in your home directory, and give the file the name `set-connection.sh`. Add the following to the new file:

```bash
# the Base URI of our automation controller cluster node
export CONTROLLER_HOST=https://autoctl1.<LABID>.<SUBDOMAIN>.opentlc.com

# the user name
export CONTROLLER_USERNAME=admin

# and the password
export CONTROLLER_PASSWORD='<VERY_SECRET_PASSWORD>'

# do not verify the SSL certificate, in production, you will use proper SSL certificates and not need this option or set it to True
export CONTROLLER_VERIFY_SSL=false
```

{{% notice warning %}}
As always make sure to replace `<LABID>`, `<SUBDOMAIN>` and `<VERY_SECRET_PASSWORD>`!
{{% /notice %}}

Then save the file. If you're using the visual editor in VSCode use **File**, **Save as**.

Now set the environment variables by sourcing the file:

```bash
source set-connection.sh
```

{{% notice warning %}}
Make sure you define the environment variables in the same shell you want to later run your Ansible Playbook from, otherwise the Playbook will fail due to authentication errors. If you lost connection to VSCode, just source the `set-connection.sh` file again.
{{% /notice %}}

## Create the Playbook

Now we'll create a Playbook with the tasks needed to configure our automation controller to run a job template. We have prepared a template already so you don't have to cobble everything together yourself from the beginning.

First download the template from our repo. Go to your VS Code terminal and run:

```bash
wget https://gitlab.com/ansible-labs-crew/ansible-labs-crew.gitlab.io/-/raw/main/content/ansible-controller-advanced/3-awx-collection-intro/configure-controller.yml
```

Let's start with having a good look at what the `configure-controller.yml` Playbook is doing. Open it, compare the tasks and edit if required:

{{% notice tip %}}
Since we are calling the REST API of automation controller, the Ansible Playbook is run against **localhost**, but the modules will connect to the URL provided by the **CONTROLLER_HOST** environment variable you set above.
{{% /notice %}}

Tasks:
* **Create an inventory**: An inventory is created
* **Add hosts to inventory**: Hosts are added to the inventory. **Replace the \<LABID> occurrences with your LABID**
* **Machine Credentials**: Machine credentials are created. **Replace the \<LABID> occurrences with your LABID**

{{% notice tip %}}
SSH keys have already been created and distributed in your lab environment and `sudo` has been setup on the managed hosts to allow password-less login. When you SSH into a host as user a regular user from bastion node you will become user **ec2-user** on the host you logged in.
{{% /notice %}}

* **Lab Project**: A Project is created, the content used in this lab is hosted on GitLab in the project [https://gitlab.com/ansible-labs-crew/playbooks-adv-controller](https://gitlab.com/ansible-labs-crew/playbooks-adv-controller).

### Add a Task to create a Template

You might have noticed the downloaded Playbook is missing one task... it's not creating a job template. This is a job for you! The task should:

* Create a Job Template task named `Lab Job Template`
* Use the `ansible.controller.job_template` module with the following parameters:
  * **name**: Install Apache
  * **organization**: Default
  * **state**: present
  * **inventory**: Lab inventory
  * **become_enabled**: True
  * **playbook**: apache_install.yml
  * **project**: Lab Project
  * **credential**: Lab Credentials

{{% notice info %}}
Use **ansible-navigator doc ansible.controller.job_template** to open the documentation for the specified module.
{{% /notice %}}

### Run the Playbook

After you have finished adding the task, save the Playbook and finally run it (make sure the environment variables are set as shown above):

```bash
ansible-navigator run configure-controller.yml
```

{{% notice tip %}}
While in `ansible-navigator` press a line number e.g. to get more information about a play or tasks. Note: this works only for less then 10 lines, if you have more you need to press `:` first. To leave press `Esc` multiple times to get up again in the views and eventually out of Navigator.
{{% /notice %}}

After the successful run log into your automation controller web UI, you will see the configured objects and a job "source code update" to synchronize the git project.

<details><summary><b>Click here for Solution</b></summary>
<hr/>
<p>

```yaml
---
- name: Configure automation controller
  hosts: localhost
  become: false
  gather_facts: false
  tasks:
  - name: Create an inventory
    ansible.controller.inventory:
      name: Lab inventory
      organization: Default
  - name: Add hosts to inventory
    ansible.controller.host:
      name: "{{  item }}"
      inventory: Lab inventory
      state: present
    loop:
      - node1.<LABID>.internal
      - node2.<LABID>.internal
      - node3.<LABID>.internal
  - name: Machine Credentials
    ansible.controller.credential:
      name: Lab Credentials
      credential_type: Machine
      organization: Default
      inputs:
        username: ec2-user
        ssh_key_data: "{{ lookup('file', '~/.ssh/<LABID>key.pem' ) }}"
  - name: Lab Project
    ansible.controller.project:
      name: Lab Project
      organization: Default
      state: present
      scm_update_on_launch: True
      scm_delete_on_update: True
      scm_type: git
      scm_url: https://gitlab.com/ansible-labs-crew/playbooks-adv-controller.git
  - name: Lab Job Template
    ansible.controller.job_template:
      name: Install Apache
      organization: Default
      state: present
      inventory: Lab inventory
      become_enabled: True
      playbook: apache_install.yml
      project: Lab Project
      credential: Lab Credentials
```

</p>
<hr/>
</details>

{{% notice info %}}
If you run this Ansible Playbook multiple times, you will notice the **ansible.controller.credential** module is not idempotent! Since we store the SSH key encrypted, the Ansible Module is unable to verify it has already been set and didn't change. This is what we want and expect from a secure system, but it also means Ansible has no means to verify it and hence overrides the SSH key or password every time the Ansible Playbook is executed.
Also note that the `credential_type` value is simply the type's name.
{{% /notice %}}
## Verify the cluster

We are working in a clustered environment. All cluster nodes are using the same PostgreSQL database to store their data. If we apply a configuration change on one node, it's automatically applied to the other nodes as well, since they use use the same PostgreSQL database as configuration source. This means, we're not synchronizing the configuration between the nodes, instead all nodes use the same database and no sync is needed.

To verify you can log on to any of the other control plane nodes in the cluster (`https://autoctl2.<LABID>.<SUBDOMAIN>.opentlc.com` or `https://autoctl3.<LABID>.<SUBDOMAIN>.opentlc.com`) from the brower, and you will be able to see the same objects that were created by running the `configure-controller.yml`, as well as the source control sync job.

## Challenge Labs

Add a task to the Ansible Playbook you wrote in this chapter to automatically run the job **Template** you created. If you don't know how to start, check out the documentation of the **ansible.controller.job_launch** module by running `ansible-navigator doc` in your VSCode terminal:

```bash
ansible-navigator doc ansible.controller.job_launch
```

<details><summary><b>Click here for Solution</b></summary>
<hr/>
<p>

This is a Challenge Lab! No solution here.

</p>
<hr/>
</details>
